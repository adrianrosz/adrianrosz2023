/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    fontFamily: {
      sans: ['Inter', 'sans-serif'],
    },
    colors: {
      white: '#FFFFFF',
      black: '#000000',
      gray: {
        light: '#fafafa',
        DEFAULT: '#e4e4e7',
        dark: '#a1a1aa',
      },
      dribble: '#ea4c89',
      gitlab: '#fc6d27',
      instagram: '#8234af',
    },
    extend: {
      animation: {
        'scale-up-fade': 'scale-up-fade 60s linear infinite',
      },
      keyframes: {
        'scale-up-fade': {
          '0%': { transform: 'scale(1) rotate(0deg)', opacity: 0 },
          '1%': { 'opacity': 1 },
          '95%': { 'opacity': 1 },
          '100%': { transform: 'scale(1.5) rotate(20deg)', opacity: 0 },
        }
      }
    },
  },
  plugins: [],
}

