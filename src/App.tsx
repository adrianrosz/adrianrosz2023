import Button from "./components/Button";
import Logo from "./components/Logo";

import bg from "./assets/bg.png";
import icoGitlab from "./assets/ico-gitlab.svg";
import icoInstagram from "./assets/ico-instagram.svg";

function App() {
    return (
        <>
            <main className="z-10 relative min-w-screen min-h-screen flex justify-center items-center flex-col p-6">
                <section
                    className="flex justify-center items-center flex-col w-full md:w-min p-6 md:p-12 bg-white/5 backdrop-blur-sm border border-white/10 rounded">

                    <Logo />

                    <section
                        className="w-full flex justify-center items-center flex-col md:flex-row gap-3 md:gap-5 mt-12 md:mt-16">
                        <Button url="https://gitlab.com/adrianrosz"
                                className="w-full md:w-min hover:border-gitlab">
                            <img src={icoGitlab} alt="Gitlab" className="w-4 h-4"/>
                            GitLab
                        </Button>

                        <Button url="https://www.instagram.com/adrianrosz/"
                                className="w-full md:w-min hover:border-instagram">
                            <img src={icoInstagram} alt="Instagram" className="w-4 h-4"/>
                            Instagram
                        </Button>

                    </section>
                </section>
            </main>
            <div className="animate-scale-up-fade fixed inset-0 w-screen h-screen z-0 bg-black">
                <img src={bg}
                     className='w-full h-full object-cover'
                     alt='Adrian Rosz'/>
            </div>
        </>
    )
}

export default App
