import {FC, ComponentProps} from 'react';
import {cn} from '../utils/css-merge';

type ButtonProps = {
    url: string
    className?: string
} & ComponentProps<"button">

const Button: FC<ButtonProps> = ({children, url, className}) => {
    const baseCss = 'flex justify-center items-center gap-3 px-6 py-2 rounded border-solid border-2 border-white/10 hover:border-white/25';

    return (
        <a href={url}
           className={cn(`${baseCss} ${className}`)}
           target="_blank"
           rel="nofollow"
        >
            {children}
        </a>
    )
}

export default Button;
