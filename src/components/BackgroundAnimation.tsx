import {FC} from 'react';
import './BackgroundAnimation.css';

const BackgroundAnimation: FC = () => {
    return (
       <>
           <div className="lines">
               <div className="line"></div>
               <div className="line"></div>
               <div className="line"></div>
           </div>
       </>
    )
}

export default BackgroundAnimation;
